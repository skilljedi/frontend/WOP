import React from 'react';
import {useSelector} from 'react-redux';
import 'bootstrap/dist/css/bootstrap.css';
import Container from 'react-bootstrap/Container';
import NavBar from '../components/navBar'
import Banner from 'components/banner';
import Details from 'components/details';
import AccountType from 'components/accountTypes';
import Footer from 'components/footer';

function Index()
{
    const isAuthenticated = useSelector(state=>state.signInReducer.isAuthenticated);
    return(
        <Container fluid style={{paddingLeft:'0px',paddingRight:'0px'}}> 
        {isAuthenticated?<NavBar.Auth/>:<NavBar.Home/>}
        <Banner/>
        <br/>
        <Details/>
        <br/><br/>
        <AccountType/>
        <br/><br/>
        <Footer/>
        </Container>
    );
}
export default Index;