import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import {useSelector} from 'react-redux';
import Container from 'react-bootstrap/Container';
import NavBar from '../components/navBar';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import CourseCard from 'components/coursecard';
import python_logo from 'images/python.png';
import Filter from 'components/filter';
import LocationHistory from 'components/locationhistory';
function ProjectList()
{
  const isAuthenticated = useSelector(state=>state.signInReducer.isAuthenticated);
  const contents = useState({id:1,name:'Project-name-I',logo:python_logo,desc:"description",progress:0},
  {id:1,name:'Project-name-I',logo:python_logo,desc:"description",progress:0},
  {id:1,name:'Project-name-I',logo:python_logo,desc:"description",progress:0});
  return(
            <Container fluid>
            {isAuthenticated?<NavBar.Auth/>:<NavBar.Home/>}  
            <Container fluid className="mt-md-5">
            <span className="text-muted ml-md-5"><LocationHistory/></span>
            <h3 className="ml-md-5"><LocationHistory/></h3>
            </Container>
            <Row className="mt-md-4 ml-md-4">
            <Col>
            <Filter.Language/>  
            </Col>
            </Row>
            <Container fluid>
            <Row>
            <Col md={9}>
            <Container>
              {contents.map((content) =>(<Col className="col-md-5"><CourseCard content={content} /></Col>))}
            </Container>
            </Col>
            <Col md={3}>
            <Filter.Project content={['Begginner','Intermediate','Advanced','Company']}/>   
            </Col>
            </Row>
            </Container>
            </Container>
  );
}
/*
class ProjectList extends Component {
    constructor()
    {
        super();
        let contents= [{id:1,name:'Project-name-I',logo:python_logo,desc:"description",progress:0},
                       {id:1,name:'Project-name-I',logo:python_logo,desc:"description",progress:0},
                       {id:1,name:'Project-name-I',logo:python_logo,desc:"description",progress:0},
                    ];  
        this.state = {contents}
    }
    render() { 
        return (
            
          );
    }
}

 */
export default ProjectList;
