import React, { useEffect } from "react";
import "bootstrap/dist/css/bootstrap.css";
import Tab from "react-bootstrap/Tab";
import Nav from "react-bootstrap/Nav";
import "styles/signin.css";
import Login from "../components/login";
import Register from "../components/register";
import {Link, Redirect} from 'react-router-dom';
import Card from "react-bootstrap/Card";
import { useSelector } from "react-redux";
import Alert from 'react-bootstrap/Alert';
import NavBar from '../components/navBar';
import  Container  from "react-bootstrap/Container";
function SignIn(props)
{
  const data = useSelector(state=>state.signInReducer);
  const isAuthenticated = useSelector(state=>state.signInReducer.isAuthenticated);
  const regData = useSelector(state=>state.registerReducer);
  useEffect(()=>{

  },[data]);
  return (
    <Container fluid>
    {isAuthenticated?<NavBar.Auth/>:<NavBar.Home/>}
    <Card id="signin-container">
      <Card.Body>
        <Tab.Container defaultActiveKey={props.register ? "signup" : "signin"}>
          <Nav fill variant="pills" id="nav-signin">
            <Nav.Item>
              <Nav.Link eventKey="signin">Login</Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link eventKey="signup">Register</Nav.Link>
            </Nav.Item>
          </Nav>
          <Tab.Content>
            <Tab.Pane eventKey="signin">
              <Login /><br/>
              <h6 align="center">OR</h6>
              <Link to="/forgotpassword" className="text-danger">Forgot Password</Link>
              {data.isAuthenticated?<Redirect to={{pathname:"/dashboard"}}/>:<></>}
            </Tab.Pane>
            <Tab.Pane eventKey="signup">
              <Register />
            </Tab.Pane>
          </Tab.Content>
        </Tab.Container>
      </Card.Body>
      {regData.error.length>0?Array.isArray(regData.error)?regData.error.map((obj)=><Alert variant="danger">{obj.msg}</Alert>):<Alert variant="danger">{regData.error}</Alert>:<></>}
      {regData.msg.length>0?<Alert variant="success">{regData.msg}</Alert>:<></>}
      {data.error.length>0?Array.isArray(data.error)?data.error.map((obj)=><Alert variant="danger">{obj.msg}</Alert>):<Alert variant="danger">{data.error}</Alert>:<></>}
      {data.msg.length>0?<Alert variant="success">{data.msg}</Alert>:<></>}
    </Card>
    </Container>
  );
}
export default SignIn;
