import React, { Component } from "react";
import Avatar from "react-avatar";
import Card from "react-bootstrap/Card";
import { Link } from "react-router-dom";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import CourseCard from "components/coursecard";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import python_logo from "images/python.png";
import work from "images/Work.png";
import Button from "react-bootstrap/Button";

const responsive = {
  superLargeDesktop: {breakpoint: { max: 4000, min: 3000 },items: 5,},
  desktop: {breakpoint: { max: 3000, min: 1024 },items: 2,},
  tablet: {breakpoint: { max: 1024, min: 464 },items: 1,},
  mobile: {breakpoint: { max: 464, min: 0 },items: 1,},
};

class Profile extends Component {
  constructor() {
    super();
    let contents = [
      {
        id: 1,
        name: "Python",
        logo: python_logo,
        desc:
          "Some quick example text to build on the card title and make up the bulk of the card's content.",
        progress: 0,
      },
      {
        id: 2,
        name: "DJango",
        logo: python_logo,
        desc:
          "Some quick example text to build on the card title and make up the bulk of the card's content.",
        progress: 0,
      },
      {
        id: 3,
        name: "MongoDB",
        logo: python_logo,
        desc:
          "Some quick example text to build on the card title and make up the bulk of the card's content.",
        progress: 0,
      },
      {
        id: 4,
        name: "Node JS",
        logo: python_logo,
        desc:
          "Some quick example text to build on the card title and make up the bulk of the card's content.",
        progress: 0,
      },
      {
        id: 5,
        name: "Express JS",
        logo: python_logo,
        desc:
          "Some quick example text to build on the card title and make up the bulk of the card's content.",
        progress: 0,
      },
      {
        id: 6,
        name: "Postman",
        logo: python_logo,
        desc:
          "Some quick example text to build on the card title and make up the bulk of the card's content.",
        progress: 0,
      },
      {
        id: 7,
        name: "Java OOP",
        logo: python_logo,
        desc:
          "Some quick example text to build on the card title and make up the bulk of the card's content.",
        progress: 0,
      },
    ];
    this.state = { contents };

    let details = "Name Of The Institute/Organisation";
    this.work = { details };

    let languages = ["Python", "C++", "Java", "HTML", "JavaScript", "SQL", "ReactJS"];
    this.data = { languages };
  }

  profile_image = { src: "" };
  userName = { name: "User Name" };

  render() {
    return (
      <Container fluid>
        <Container
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "25vh",
          }}
        >
          <Avatar
            alt="Profile Picture"
            src={this.profile_image.src}
            name={this.userName.name}
            size="150"
            round
          />
        </Container>

        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "5vh",
          }}
        >
          <h1>{this.userName.name}</h1>
        </div>

        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "5vh",
          }}
        >
          {this.data.languages.map((lang) => (
            <div style={{ paddingLeft: "10px" }}>
              <Button
                className="btn btn-danger btn-sm"
                style={{
                  borderRadius: "20px",
                  height: "25px",
                  fontSize: "10px",
                  textAlign: "match-parent",
                  marginTop: "20px",
                }}
                content={lang}
              >
                {lang}
              </Button>
            </div>
          ))}
        </div>
        <Container fluid>
          <Container className="clearfix mt-5 mb-2">
            <h4 className="float-left">Achievements</h4>
            <Link className="float-right text-uppercase" to="/">
              see all
            </Link>
          </Container>

        <Container className="col-md-10">
          <Carousel responsive={responsive}>
            {this.state.contents.map((content) => (
              <Col>
                <CourseCard content={content} />
              </Col>
            ))}
          </Carousel>
        </Container>

          <Container className="clearfix mt-5 mb-2">
            <h4 className="float-left">Work and Experience</h4>
            <Link className="float-right text-uppercase" to="/">
              <p>Modify</p>
            </Link>
          </Container>

          <Container
            className="card w-100 shadow p-3 mb-5"
            style={{
              /*width:'30rem', height:'265px',*/ borderRadius: "1.5rem",
              borderColor: "light grey",
            }}
          >
            <Row>
              <Col
                style={{
                  width: "100px",
                  height: "100px",
                  maxWidth: "10%",
                  padding: "20px 25px 100px 25px"
                  // 30px 90px 100px 30px",
                }}
              >
                <Card.Img
                  src={work}
                  style={{ width: "100px", height: "100px" }}
                />
              </Col>
              <Col>
                <Card.Body>
                  <Card.Title>
                    <h6>{this.work.details}</h6>
                  </Card.Title>
                  <Card.Text>
                    An institution is a social structure in which people
                    cooperate and which influences the behavior of people and
                    the way they live. An institution has a purpose.
                    Institutions are permanent, which means that they do not end
                    when one person is gone. An institution has rules and can
                    enforce rules of human behavior.
                  </Card.Text>
                </Card.Body>
                <br></br>
              </Col>
            </Row>
          </Container>

          <Container className="clearfix mt-5 mb-2">
            <h4 className="float-left">Educational Qualifications</h4>
            <Link className="float-right text-uppercase" to="/">
              <p>Modify</p>
            </Link>
          </Container>

          <Container
            className="card w-100 shadow p-3 mb-5"
            style={{
              /*width:'30rem', height:'265px',*/ borderRadius: "1.5rem",
              borderColor: "light grey",
            }}
          >
            <Row>
              <Col
                style={{
                  width: "100px",
                  height: "100px",
                  maxWidth: "10%",
                  padding: "20px 25px 100px 25px",
                }}
              >
                <Card.Img
                  src={work}
                  style={{ width: "100px", height: "100px" }}
                />
              </Col>
              <Col>
                <Card.Body>
                  <Card.Title>
                    <h6>Qualifications</h6>
                  </Card.Title>
                  <Card.Text>
                    A pass of an examination or an official completion of a
                    course, especially one conferring status as a recognized
                    practitioner of a profession or activity.
                  </Card.Text>
                </Card.Body>
                <br></br>
              </Col>
            </Row>
          </Container>
        </Container>
      </Container>
    );
  }
}
export default Profile;
