import React,{useState} from "react";
import Alert from 'react-bootstrap/Alert';
import Form from 'react-bootstrap/Form';
import "styles/signin.css";
import Card from "react-bootstrap/Card";
import NavBar from '../components/navBar';
import Button from "react-bootstrap/Button"
import { Link } from "react-router-dom";
import  Container  from "react-bootstrap/Container";
import { useSelector } from "react-redux";
//import {useDispatch} from 'react-redux';
function ForgotPassword(props)
{
  const isAuthenticated = useSelector(state=>state.signInReducer.isAuthenticated);
  const [data,setData] = useState(null);
  const [error,setErrors] = useState({email:''});
    
  //  const dispatch = useDispatch();
    const handleChange = (event)=>{
        let regexEmail = RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);
        const {name,value} = event.target; 
        switch(name)
        {
            case 'email':
                regexEmail.test(value)?setErrors({...error,email:''}):setErrors({...error,email:'Email Not Valid'});
                break;  
            default: break;
        }
        setData({...data,[name]:value})
    }
    const handleSubmit=(event)=>{
        event.preventDefault()
      if(validateForms(error))
      {
       // dispatch(signIn(data))
      }
       }
      const validateForms = (errors)=>{
        let valid = true;
        if(!errors.email==="")valid = false;
        return valid;
      }
  return (
    <Container fluid>
    {isAuthenticated?<NavBar.Auth/>:<NavBar.Home/>}
    <Card id="signin-container">
    <Card.Body>
     <h2 align="center">Forgot Password</h2>
     <br/>
     <Form onSubmit={handleSubmit}>
     <Form.Label>Email</Form.Label>
            <Form.Control type="email" placeholder="Email" onChange={handleChange} name="email" required/>
            {error.email.length>0&&<Alert variant="danger">{error.email}</Alert>}<br/>    
            <Button
            type="submit"
            variant="danger"
            block
            disabled={false}
          >Submit</Button>
     </Form>
     <br/>
     <h6 align="center">OR</h6>
     <Link to="/signin" align="center" className="text-danger">Sign In</Link>
    </Card.Body>
    </Card>
    </Container>
  );
}

export default ForgotPassword;