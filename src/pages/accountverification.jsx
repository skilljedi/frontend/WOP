import React, { useEffect } from 'react';
import Container from 'react-bootstrap/Container';
import "bootstrap/dist/css/bootstrap.min.css";
import Row from 'react-bootstrap/Row';
import checkVerification from '../redux/actions/verificationAction';
import NavBar from '../components/navBar';
import success_img from '../images/Success.png'
import failed_img from '../images/Failed.png'
import {useParams} from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
function AccountVerification(props)
{
    const token = useParams();
    const data = useSelector(state=>state.verificationReducer)
    const dispatch = useDispatch();
    useEffect(()=>{
     dispatch(checkVerification(token.id))   
    },[])
    return(
        <Container fluid>
        <NavBar.Verification/>
        <Container className="justify-content-center mt-5">
        <Row className="justify-content-center">
        <h4>{data.verified?data.msg:data.error}</h4>
        </Row>
        <Row className="justify-content-center mt-3">
         <img src={data.verified?success_img:failed_img} alt="state" height="100" width="100"/>   
         </Row>   
         </Container>
        </Container>    
    ) 
}

export default AccountVerification;