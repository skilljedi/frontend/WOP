import React, {useState} from "react";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import "bootstrap/dist/css/bootstrap.css";
import Container from "react-bootstrap/Container";
import {useSelector} from 'react-redux';
import NavBar from '../components/navBar';
import Col from "react-bootstrap/Col";
import CourseCard from "components/coursecard";
import {courses,projects} from '../dashboard'
import { Link} from "react-router-dom";
//user cannot go back to main page after login
function DashBoard()
{
  const responsive = {superLargeDesktop: {breakpoint: { max: 4000, min: 3000 },items: 5,},
    desktop: {breakpoint: { max: 3000, min: 1024 },items: 2,},tablet: {breakpoint: { max: 1024, min: 464 },items: 1,},
    mobile: {breakpoint: { max: 464, min: 0 },items: 1,},
  };
  const isAuthenticated = useSelector(state=>state.signInReducer.isAuthenticated);
  const coursesObj = useState(courses);
  const projectsObj = useState(projects);
    return( 
    <Container fluid> 
    {isAuthenticated?<NavBar.Auth/>:<NavBar.Home/>}
    <Container className="clearfix mt-5 mb-2">
      <h4 className="float-left">Projects</h4>
      <Link className="float-right text-uppercase" to="/">
        see all
      </Link>
    </Container>
    <Container className="col-md-10">
      <Carousel responsive={responsive}>
        {coursesObj.map((course) => (
          <Col>
            <CourseCard content={course}/>
          </Col>
        ))}
      </Carousel>
    </Container>

    <Container className="clearfix mt-5 mb-2">
      <h4 className="float-left">Courses</h4>
      <Link className="float-right text-uppercase" to="/">
        see all
      </Link>
    </Container>

    <Container className="col-md-10 col-md-10">
      <Carousel responsive={responsive}>
        {projectsObj.map((project) => (
          <Col>
            <CourseCard content={project} />
          </Col>
        ))}
      </Carousel>
    </Container>
    </Container>
    );
  }
export default DashBoard;
