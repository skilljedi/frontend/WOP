import React, {useEffect} from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import CourseCard from 'components/coursecard';
import ProgressBar from 'react-bootstrap/ProgressBar';
import {useSelector} from 'react-redux';
import { useDispatch } from "react-redux";
import NavBar from '../components/navBar'
import fetchCourse from '../redux/actions/courseAction';
import Alert from 'react-bootstrap/Alert';
import Filter from 'components/filter';
import LocationHistory from 'components/locationhistory'; 
function CourseList(props)
{
    const data = useSelector(state=>state.courseReducer);
    const isAuthenticated = useSelector(state=>state.signInReducer.isAuthenticated);
    const dispatch = useDispatch(); 

    useEffect(()=>{
        dispatch(fetchCourse())
    },[])
    return (
        <Container fluid>
        {isAuthenticated?<NavBar.Auth/>:<NavBar.Home/>}
        {data.loading?<ProgressBar animated now={100} variant="danger"/>:<p></p>}         
        <Container fluid className="mt-md-5">
        <span className="text-muted ml-md-5"><LocationHistory/></span>
        <h3 className="ml-md-5"><LocationHistory/></h3>
        </Container>
        <Row className="mt-md-4 ml-md-4">
        <Col>
        <Filter.Language/>  
        </Col>
        <Col>
        </Col>
        <Col>
        <Filter.Course/>
        </Col>
        </Row>
        <Container fluid>
        <Row className="ml-md-4">    
        {       
        data.courses.length===0?
        (<Alert variant="danger" className="mt-5">{data.error}</Alert>):
        (
          data.courses.map((content)=><Col className="ml-md-2"><CourseCard content={content}/></Col>)
        )
        }          
        </Row>
        </Container>
        </Container>
      );
}
export default CourseList;