import {createStore,applyMiddleware,combineReducers} from 'redux';
import courseReducer from './reducers/courseReducer';
import signInReducer from './reducers/signinReducer';
import verificationReducer from './reducers/verificationReducer';
import registerReducer from './reducers/registerReducer';
import thunk from 'redux-thunk';
const reducer =combineReducers({
    courseReducer:courseReducer,
    signInReducer:signInReducer,
    verificationReducer:verificationReducer,
    registerReducer:registerReducer
})
const store = createStore(reducer,applyMiddleware(thunk));
export default store;