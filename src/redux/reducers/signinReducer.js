import {SIGN_IN_SUCCESS,SIGN_IN_REQUEST,SIGN_IN_FAILURE,FSIGN_IN_SUCCESS,LOG_OUT} from 'redux/actions/signinTypes';
const initialState = 
{
    loading:false,
    error:[],
    msg:[],
    isAuthenticated:false
}

const signInReducer = (state=initialState,action)=>
{
    switch(action.type)
    {
        case SIGN_IN_SUCCESS:return{
            ...state,
            msg:action.msg,
            isAuthenticated:true,
        }
        case FSIGN_IN_SUCCESS:return{
             ...state,
             msg:action.msg,
        }
        case SIGN_IN_REQUEST:return{
            ...state,
            loading:true
        }

        case SIGN_IN_FAILURE:return{
            ...state,
            error:action.error,
        }
        case LOG_OUT:return{
            ...state,
            isAuthenticated:false,
            msg:action.msg
        }
        default: return state
    }

}

export default signInReducer;