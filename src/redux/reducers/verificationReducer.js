import {VERIFIED_SUCESSFULLY,VERIFYING_FAILED,VERIFYING} from '../actions/verificationTypes';
const initialState=
{
    loading:false,
    msg:'',
    error:'',
    verified:false
} 

const verificationReducer = (state=initialState,action)=>
{
    switch(action.type)
    {
        case VERIFYING:return{
            ...state,
            loading:true
        }
        case VERIFIED_SUCESSFULLY:return{
            ...state,
            error:'',
            verified:true,
            msg:action.payload,
        }

        case VERIFYING_FAILED:return{
            ...state,
            msg:'',
            verified:false,
            error:action.payload,
        }
        default:return state
    }
} 

export default verificationReducer;