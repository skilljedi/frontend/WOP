import {REGISTER_SUCCESS,REGISTER_FAILED,REGISTERING} from 'redux/actions/registerTypes'

const initialState = {
    loading:false,
    msg:'',
    error:[]
}

const registerReducer = (state=initialState,action)=>
{
    switch (action.type) {
        case REGISTER_SUCCESS:return{
            ...state,
            msg:action.msg
        }
            
        case REGISTER_FAILED:return{
            ...state,
            error:action.err
        }

        case REGISTERING:return{
            ...state,
            loading:true
        }
        default: return state;
    }

}

export default registerReducer;