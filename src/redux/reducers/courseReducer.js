import {FETCH_COURSE_FAILURE,FETCH_COURSE_REQUEST,FETCH_COURSE_SUCCESS} from '../actions/courseTypes';
const initialState={

    loading:false,
    courses:[],
    error:'',
    language:''
    
}

const courseReducer = (state=initialState,action)=>
{
    switch(action.type)
    {
        case FETCH_COURSE_REQUEST: return{
            ...state,
            loading:true
        }

        case FETCH_COURSE_SUCCESS: return{
            loading:false,
            error:'',
            courses:action.payload,
        }
        case FETCH_COURSE_FAILURE:return{
            loading:false,
            courses:[],
            error:action.payload,
        }
        default: return state
    }

}



export default courseReducer;