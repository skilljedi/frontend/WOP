import axios from 'axios';
import {VERIFIED_SUCESSFULLY,VERIFYING_FAILED,VERIFYING} from '../actions/verificationTypes';

const verfying =()=>
{
    return{
        type:VERIFYING
    }


}
const verifiedSucessfully = (message)=>
{
    return{
        type:VERIFIED_SUCESSFULLY,
        payload:message
    }
}

const verifyingFailed = (error)=>
{
    return{
        type:VERIFYING_FAILED,
        payload:error
    }
} 

const checkVerification=(token)=>{
    return function(dispatch){
        dispatch(verfying())
        axios.post('http://localhost:3001/users/verify',{
        token:token  
        })
        .then(res=>{
            dispatch(verifiedSucessfully(res.data))
        })
        .catch(error=>{
            let message; 
            message= error.response ===undefined? "Network Unavailable":error.response.data;
           dispatch(verifyingFailed(message))
        })
    }
}

export default checkVerification;
 