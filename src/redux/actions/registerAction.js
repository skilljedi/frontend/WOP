import axios from 'axios';
import {REGISTER_SUCCESS,REGISTER_FAILED,REGISTERING} from './registerTypes';

const registerSuccess=(msg)=>{
    return{
        type:REGISTER_SUCCESS,
        msg:msg
    }
}

const registerFailed=(err)=>{

    return{
        type:REGISTER_FAILED,
        err:err
    }

}

const registering= ()=>
{
    return{
        type:REGISTERING,
    }
}

const register = (data)=>{

    return function (dispatch) {
        dispatch(registering())
        axios.post('http://localhost:3001/users/register',data)
        .then(res=>{
            console.log(res)
            dispatch(registerSuccess(res.data))
        })
        .catch(error=>{
            let message = []; 
            message= error.response ===undefined? "Network Unavailable":error.response.data;
            dispatch(registerFailed(message)) 
        })
    }
}

export default register;