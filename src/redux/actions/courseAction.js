import {FETCH_COURSE_FAILURE,FETCH_COURSE_REQUEST,FETCH_COURSE_SUCCESS} from './courseTypes';
import axios from 'axios';

const fetchCourseRequest = ()=>
{
    return{
        type:FETCH_COURSE_REQUEST
    }
}

const fetchCourseFailure = (error)=>
{
    return {
        type:FETCH_COURSE_FAILURE,
        payload:error
    }

}

const fetchCourseSuccess = (courses)=>
{
    return{
        type:FETCH_COURSE_SUCCESS,
        payload:courses
    }
}
const fetchCourse = ()=>
{
    return function (dispatch){
        dispatch(fetchCourseRequest())
        axios.get('http://localhost:3001/courses')
        .then(res=>{
            dispatch(fetchCourseSuccess(res.data))
        })
        .catch(error=>{
            let errmsg = error.response===undefined? "Server Error" :error.response.data 
            dispatch(fetchCourseFailure(errmsg))
        })
    }
}

export default fetchCourse;