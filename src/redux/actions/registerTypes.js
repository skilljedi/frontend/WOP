const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
const REGISTER_FAILED = 'REGISTER_FAILED';
const REGISTERING = 'REGISTERING';

export  {REGISTER_SUCCESS,REGISTER_FAILED,REGISTERING}