import {SIGN_IN_SUCCESS,SIGN_IN_REQUEST,SIGN_IN_FAILURE,FSIGN_IN_SUCCESS,LOG_OUT} from './signinTypes';
import axios from 'axios';
const signInSuccess= (msg)=>{

    return {
        type:SIGN_IN_SUCCESS,
        msg:msg
    }
}
const signInRequest = () =>
{
    return{
        type:SIGN_IN_REQUEST
    }
}

const signInFailure = (error)=>
{
    return{
        type:SIGN_IN_FAILURE,
        error:error
    }
}

const signinPartialSucess=(msg)=>
{
    return{
        type:FSIGN_IN_SUCCESS,
        msg:msg
    }

}
const logOut =(msg)=>
{
    return {
        type:LOG_OUT,
        msg:msg
    }
}
const signIn = (data)=>{
     
    return function (dispatch)
    {
        dispatch(signInRequest())
        axios.post('http://localhost:3001/auth',data)
        .then(res=>{
            if(res.data.token)
            {
                const token =  res.data.token;
                delete res.data.token;
                console.log(token)
                localStorage.setItem('x-auth-token',token);
                dispatch(signInSuccess("Sign In Sucess"))
            } 
            else
            {
               dispatch(signinPartialSucess("Sorry Something Went Wrong"))
            }
            return res
        })
        .catch(error=>{
            let message = []; 
            message= error.response ===undefined? "Network Unavailable":error.response.data;
            dispatch(signInFailure(message))
        })
        
    }

} 
const signOut = (msg) =>{
    return function(dispatch)
    {
        localStorage.removeItem('x-auth-token')
        dispatch(logOut())
    }
}
export {signOut};

export default signIn;