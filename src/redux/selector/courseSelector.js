export const getCourseState = store => store.courseReducer

export const getCourseList = store => 
 getCourseState(store) ? getCourseState(store).courses : [];

 export const getCourseByLanguage = (store,language) =>
  getCourseState(store).language===language ? getCourseState(store).course :[]; 