import React from 'react';
import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/Dropdown';
import Form from 'react-bootstrap/Form';
import CustomMenu from './customMenu';
import Button from 'react-bootstrap/Button';
const Filter =
{
  Project: function Project(props)
           {
            return(
              <> 
             <h4>Filter</h4>  
             <Form.Group>
              {props.content.map((label)=>
              {
               return(
              <Form.Row className="p-2">    
              <Form.Check type="checkbox" custom label={label} id={"default-checkbox"+label}/>
              </Form.Row>
               ); 
              })};    
             </Form.Group>
             </>
             );
           },
  Course:function Course(props)
         {
           return(
             <>
              <DropdownButton variant="danger" id="dropdown-basic-button" title="Filter">
              <Dropdown.Item href="?basedonprofile">Based on Profile</Dropdown.Item>
              </DropdownButton>
             </>
           )
         },
  
  Language:function Language(props)
           {
            const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
              <a
                href=""
                ref={ref}
                onClick={(e) => {
                  e.preventDefault();
                  onClick(e);
                }}
                style={{color:'white'}} 
              >
                {children}
                &#x25bc;
              </a>
            ));
return(
  <Dropdown as={Button} style={{backgroundColor:'#dc3545'}}>
    <Dropdown.Toggle as={CustomToggle} id="dropdown-custom-components">
      Language
    </Dropdown.Toggle>
    <Dropdown.Menu as={CustomMenu}>
      <Dropdown.Item eventKey="1">Python</Dropdown.Item>
      <Dropdown.Item eventKey="2">Java</Dropdown.Item>
      <Dropdown.Item eventKey="3">
        Javascript
      </Dropdown.Item>
    </Dropdown.Menu>
  </Dropdown>
    )
           }       
}

export default Filter;