import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import {useDispatch} from 'react-redux';
import signOut from '../redux/actions/signinAction';
import Button from 'react-bootstrap/Button';
import brand_logo from 'images/wop_logo.png';
import Dropdown from 'react-bootstrap/Dropdown';
import "bootstrap/dist/css/bootstrap.min.css";
import {Link } from "react-router-dom";  
const NavBar = 
{
    Home:function Home(props)
    {
        return(
            <Navbar expand="lg" variant="dark" bg="dark">
            <Navbar.Brand href="#home">
              <Link to="/">
                <img src={brand_logo} width="110" height="43" alt="Logo" />
              </Link>
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className=" mr-md-auto">
                <Nav.Item className="ml-md-5">
                  <Link
                    to="/Courses"
                    className="nav-link text-white">
                    {" "}
                    Courses
                  </Link>
                </Nav.Item>
                <Nav.Item className="ml-md-5">
                  <Link
                    to="/Projects"
                    className="nav-link text-white"
                    >
                    {" "}
                    Projects
                  </Link>
                </Nav.Item>
                <Nav.Item className="ml-md-5">
                  <Link className="nav-link text-white">Learning Path</Link>
                </Nav.Item>
                <Nav.Item className="ml-md-5">
                  <Link className="nav-link text-white">About</Link>
                </Nav.Item>
                <Form
                  inline
                  className="ml-md-5"
                  >
                  <FormControl
                    type="text"
                    placeholder="Search"
                    className="mr-sm-2"
                  />
                  <Button
                    variant="outline-danger"
                    type="submit"
                    value="submit"
                    >
                    Search
                  </Button>
                </Form>
              </Nav>
              <Nav>
                <Nav.Item className="mr-md-5">
                  <Link to="/signin" className="nav-link text-white">
                     Sign In
                  </Link>
                </Nav.Item>
                <Nav.Item>
                  <Button variant="danger">
                    <Link to="/signup" className="text-white">
                      Sign Up
                    </Link>
                  </Button>
                </Nav.Item>
              </Nav>
            </Navbar.Collapse>
          </Navbar>      
        )
    },
    Auth:function Auth(props)
    {
      const dispatch = useDispatch(); 
        return(
            <Navbar expand="lg" variant="dark" bg="dark">
            <Navbar.Brand href="#home">
              <Link to="/">
                <img src={brand_logo} width="110" height="43" alt="Logo" />
              </Link>
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className=" mr-md-auto">
                <Nav.Item className="ml-md-5">
                  <Link
                    to="/Dashboard"
                    className="nav-link text-white">
                    {" "}
                    Dashboard
                  </Link>
                </Nav.Item>
                <Nav.Item className="ml-md-5">
                  <Link
                    to="/Projects"
                    className="nav-link text-white"
                    >
                    {" "}
                    Projects
                  </Link>
                </Nav.Item>
                <Nav.Item className="ml-md-5">
                  <Link to="/Courses" className="nav-link text-white">Courses</Link>
                </Nav.Item>
                <Form
                  inline
                  className="ml-md-5"
                  >
                  <FormControl
                    type="text"
                    placeholder="Search"
                    className="mr-sm-2"
                  />
                  <Button
                    variant="outline-danger"
                    type="submit"
                    value="submit"
                    >
                    Search
                  </Button>
                </Form>
              </Nav>
              <Nav>
                <Nav.Item>
                <Dropdown>
                <Dropdown.Toggle id="dropdown-basic" variant="dark">
                  Username
                </Dropdown.Toggle>
                <Dropdown.Menu>
                <Link to="/Profile">    
                <Dropdown.Item href="#/action-1">Profile</Dropdown.Item>
                </Link>
                <Dropdown.Item href="#/action-2">Settings</Dropdown.Item>
                <Dropdown.Item onclick={dispatch(signOut("Sign Out Sucess"))}>Logout</Dropdown.Item>
                </Dropdown.Menu>
                </Dropdown>       
                </Nav.Item>
              </Nav>
            </Navbar.Collapse>
          </Navbar> 
        )
    },
    Verification:function Verification(props)
    {
      return(
      <Navbar expand="lg" variant="dark" bg="dark">
          <img src={brand_logo} width="110" height="43" alt="Logo" className="mx-auto d-block"/>
      </Navbar>
      )
    }
            
    
    
}

export default NavBar;