import React, { Component } from 'react';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import 'bootstrap/dist/css/bootstrap.css';
import 'styles/course.css';
class CourseCard extends Component {
    constructor(props)
    {
        super(props);
        this.state ={}
    }
    render() { 
        return (
            <Row className="course-card mt-md-5 p-2">    
            <Col style={{width:'100px',height:'100px',maxWidth:'20%',paddingTop:'40px'}}>
            <Card.Img src={this.props.content.logo} style={{width:'100px',height:'100px'}}/>  
            </Col>
            <Col>
            <Card.Body>
            <Card.Header><h4>{this.props.content.name}</h4></Card.Header>
            <Card.Text>
            {this.props.content.description}   
            </Card.Text>
            </Card.Body>
            <Card.Footer>        
            <Button variant="danger">Launch</Button>
            </Card.Footer>
            </Col>    
            </Row>
        );
    }
}
 
export default CourseCard;