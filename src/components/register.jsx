import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import register from '../redux/actions/registerAction';
import Alert from 'react-bootstrap/Alert';
import {useSelector} from 'react-redux';
import { useDispatch } from 'react-redux';
function Register(props)
{
    const [data,setData] = useState(null);
    const [error,setErrors] = useState({username:'',email:'',password:''});
    //const backendError = useSelector(state=>state.registerReducer.error.);
    const dispatch = useDispatch();
    const handleChange = (event)=>{
        event.preventDefault();  
        let regexPassword = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$.!%*#?&])[A-Za-z\d@$.!%*#?&]{8,}$/;
        let regexEmail = RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);
        const {name,value} = event.target; 
        switch(name){
          case 'username':
                value.length<6?setErrors({...error,username:'Username should not be less than 6'}):setErrors({...error,username:''});
                break;   
          case 'email':
            regexEmail.test(value)?setErrors({...error,email:''}):setErrors({...error,email:'Email Not Valid'});
             break;
          case 'password':
            regexPassword.test(value)?setErrors({...error,password:''}):setErrors({...error,password:'minimum eight characters, at least one letter, one number and one special character'});  
            break;
          default: break;
        }
        setData({...data,[name]:value})
        }
        const handleSubmit=(event)=>{
            event.preventDefault();
            if(validateForms(error))
            {
             dispatch(register(data))
            }
            }
        const validateForms = (errors)=>{
            let valid = true;
            if(errors.email.length>0||errors.password.length>0||errors.username.length>0)valid = false;
            return valid;
            }     
        return (
            <React.Fragment>
                <h2 align="center">Register</h2>
                <Form onSubmit={handleSubmit}>
                        <Form.Label>Username</Form.Label>
                        <Form.Control type="text" placeholder="Username" name="username" onChange={handleChange} required/><br/>
                        {error.username.length>0&&<Alert variant="danger">{error.username}</Alert>}
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="email" placeholder="Email" name="email" onChange={handleChange} required/><br/>
                        {error.email.length>0&&<Alert variant="danger">{error.email}</Alert>}
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" name="password" onChange={handleChange} required/><br/>
                        {error.password.length>0&&<Alert variant="danger">{error.password}</Alert>}
                        <br />
                    <Button variant="danger" type="submit" block>
                        Register
                </Button>
                </Form>
            </React.Fragment>
        );
    }
export default Register;
