import React, { Component } from 'react';
import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import lang_course from 'images/lang_course.png';
import proj_course from 'images/download.png';
import proj_expert from 'images/project_manager.svg';
import git_logo from 'images/git_logo.png';
import Container from 'react-bootstrap/Container';
import "bootstrap/dist/css/bootstrap.min.css";
import "styles/details.css";
class Details extends Component {
    state = {  }
    render() { 
        let medium_url="https://medium.com/@ashk3l/a-visual-introduction-to-git-9fdca5d3b43a";
        return ( 
            <Container fluid>
            <Row><Col md={12}><h3 align="center">Why WoP?</h3></Col></Row>
            <br/>    
            <Row className="ml-md-4 ml-sm-3">
            <Col  md={3} className="col-details">    
            <Card style={{ width: '14rem' }}>
            <Card.Img variant="top" src={lang_course} width="120" height="120"/>
            <Card.Body>
            <Card.Title>Language Preference</Card.Title>
            <Card.Text>
            Courses based on Languages that you<br/>
            are confident on
            </Card.Text>
            </Card.Body>
            </Card>
            </Col> 
            <Col  md={3} className="col-details"> 
            <Card style={{ width: '14rem' }} md={3}>
            <Card.Img variant="top" src={proj_course} width="120" height="120"/>
            <Card.Body>
            <Card.Title>Project Oriented Courses</Card.Title>
            <Card.Text>
            Courses with most used frameworks or libraries in<br/>
            specific domain 
            </Card.Text>
            </Card.Body>
            </Card>
            </Col>
            <Col  md={3} className="col-details"> 
            <Card style={{ width: '14rem' }} md={3}>
            <Card.Img variant="top" src={proj_expert} width="120" height="120"/>
            <Card.Body>
            <Card.Title>Wide Variety of Projects</Card.Title>
            <Card.Text>
            Become a Project Master by taking all types<br/>
            of projects from begginner to advanced
            </Card.Text>
            </Card.Body>
            </Card>
            </Col>
            <Col  md={3} className="col-details"> 
            <Card style={{ width: '14rem' }} md={3}>
            <Card.Img variant="top" src={git_logo} width="100" height="120"/>
            <Card.Body>
            <Card.Title>Version Control System Integrated</Card.Title>
            <Card.Text>
            Your projects are professionally <br/>
            managed through version control system
            </Card.Text>
            <Card.Link href={medium_url} className="ml-md-6">Learn More</Card.Link>
            </Card.Body>
            </Card>
            </Col>
            </Row>
            </Container>
         );
    }
}
 
export default Details;