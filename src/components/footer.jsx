import React, { Component } from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Nav from 'react-bootstrap/Nav';
import Dropdown from 'react-bootstrap/Dropdown';
import Image from 'react-bootstrap/Image';
import brand_logo from 'images/wop_logo.png';
import 'bootstrap/dist/css/bootstrap.css';
import 'styles/footer.css';
class Footer extends Component {
    state = {}
    render() {
        return (
            <Container fluid>
                <Row style={{ backgroundColor: '#21212a', padding: '10px 0px 40px 5px' }}>
                    <Col md={4}>
                        <Nav.Link id="nav-footer" href="courses">Courses</Nav.Link>
                        <Nav.Link id="nav-footer" href="projects">Projects</Nav.Link>
                    </Col>
                    <Col md={4}>
                        <Nav.Link id="nav-footer" href="Aboutus">About Us</Nav.Link>
                        <Nav.Link id="nav-footer" href="faq">FAQ</Nav.Link>
                        <br />
                        <Image src={brand_logo} width="110" height="43" alt="brand-logo" className="ml-md-1" />
                        <span className="text-white">Copyright  2020</span>
                    </Col>
                    <Col md={4}>
                        <Dropdown className="ml-md-4">
                            <Dropdown.Toggle id="drop-down-language">
                                En
            </Dropdown.Toggle>
                            <Dropdown.Menu>
                                <Dropdown.Item href="#/action-1">ML</Dropdown.Item>
                                <Dropdown.Item href="#/action-2">HI</Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default Footer;