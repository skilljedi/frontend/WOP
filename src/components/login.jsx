import React,{useState} from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import "bootstrap/dist/css/bootstrap.css";
import Alert from 'react-bootstrap/Alert';
import { useDispatch } from "react-redux";
import signIn from "../redux/actions/signinAction";
//TODO during sigin show progressbar stating its trying to sign in
function Login(props)
{
  const [data,setData] = useState(null);
  const [error,setErrors] = useState({email:''});
  const dispatch = useDispatch();
  const handleChange = (event)=>{
  event.preventDefault(); 
  let regexEmail = RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);
  const {name,value} = event.target; 
  switch(name){
    case 'email':
      regexEmail.test(value)?setErrors({...error,email:''}):setErrors({...error,email:'Email Not Valid'});
       break;  
    default: break;
  }
  setData({...data,[name]:value})
  }
  const handleSubmit=(event)=>{
    event.preventDefault()
  if(validateForms(error))
  {
    dispatch(signIn(data))
  }
   }
  const validateForms = (errors)=>{
    let valid = true;
    if(!errors.email==="")valid = false;
    return valid;
  }
  return(
    <React.Fragment>
        <h2 align="center">Login</h2>
        <Form onSubmit={handleSubmit}>
            <Form.Label>Email</Form.Label>
            <Form.Control type="email" placeholder="Email" onChange={handleChange} name="email" required/>
            {error.email.length>0&&<Alert variant="danger">{error.email}</Alert>}<br/>
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" placeholder="Password" onChange={handleChange} required name="password"/>
          <br/>
          <Button
            type="submit"
            variant="danger"
            block
            disabled={false}
          >
            Login
          </Button>
        </Form>
      </React.Fragment>
  );
}
export default Login;
