import React, { Component } from 'react';
import Image from 'react-bootstrap/Image';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import banner from 'images/team_work_bl.jpg';
import 'styles/banner.css';
import {
    Link
  } from "react-router-dom";
class Banner extends Component {
    
    render() { 
        return (
              <Container fluid className="banner" style={{paddingLeft:'0px',paddingRight:'0px'}}>            
              <Image src={banner} alt="Banner" className="img-banner"/>
              <Container className="over-image-text" fluid>
              <h1 className="text-white">Want to work on a project?</h1>
              <h3 className="text-white">But don't know where to start?</h3><br/>
              <Link to="/signup" className="text-white">
              <Button variant="light">Sign Up</Button><br/>
              </Link>
              </Container>
              </Container>        
            );
    }
}
 
export default Banner;