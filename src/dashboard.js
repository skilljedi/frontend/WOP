import python_logo from "images/python.png";
import course_logo from "images/lang_course.png";
let projects = [
    {
      id: 1,
      name: "Project-name-I",
      logo: python_logo,
      desc: "description",
      progress: 0,
    },
    {
      id: 2,
      name: "Project-name-II",
      logo: python_logo,
      desc: "description",
      progress: 0,
    },
    {
      id: 3,
      name: "Project-name-III",
      logo: python_logo,
      desc: "description",
      progress: 0,
    },
    {
      id: 4,
      name: "Project-name-IV",
      logo: python_logo,
      desc: "description",
      progress: 0,
    },
    {
      id: 5,
      name: "Project-name-V",
      logo: python_logo,
      desc: "description",
      progress: 0,
    },
    {
      id: 6,
      name: "Project-name-VI",
      logo: python_logo,
      desc: "description",
      progress: 0,
    },
    {
      id: 7,
      name: "Project-name-VII",
      logo: python_logo,
      desc: "description",
      progress: 0,
    },
    {
      id: 8,
      name: "Project-name-VIII",
      logo: python_logo,
      desc: "description",
      progress: 0,
    },
  ];

  let courses = [
    {
      id: 1,
      name: "Course-name-I",
      logo: course_logo,
      desc: "description",
      progress: 0,
    },
    {
      id: 2,
      name: "Course-name-II",
      logo: course_logo,
      desc: "description",
      progress: 0,
    },
    {
      id: 3,
      name: "Course-name-III",
      logo: course_logo,
      desc: "description",
      progress: 0,
    },
    {
      id: 4,
      name: "Course-name-IV",
      logo: course_logo,
      desc: "description",
      progress: 0,
    },
    {
      id: 5,
      name: "Course-name-V",
      logo: course_logo,
      desc: "description",
      progress: 0,
    },
    {
      id: 6,
      name: "Course-name-VI",
      logo: course_logo,
      desc: "description",
      progress: 0,
    },
    {
      id: 7,
      name: "Course-name-VII",
      logo: course_logo,
      desc: "description",
      progress: 0,
    },
    {
      id: 8,
      name: "Course-name-VIII",
      logo: course_logo,
      desc: "description",
      progress: 0,
    },
  ];

  export {projects,courses}