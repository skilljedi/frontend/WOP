import React, { useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Container from "react-bootstrap/Container";
import Profile from "pages/profile";
import DashBoard from "pages/dashboard";
import CourseList from "pages/courselist";
import ProjectList from "pages/projectlist";
import ForgotPassword from 'pages/forgotpassword';
import AccountVerfication from 'pages/accountverification';
import SignIn from "pages/signin";
import Index from "pages/index";
import { BrowserRouter as Router, Switch, Route} from "react-router-dom";
//Get location to disable nav bar in mail verification page
function App(props)
{
  return(
    <Container fluid>
        <Router>
          <Switch>
            <Route exact path="/">
              <Index />
            </Route>
            <Route exact path="/Courses">
              <CourseList />
            </Route>
            <Route exact path="/Projects">
              <ProjectList />
            </Route>
          </Switch>
          <Route exact path="/Signin">
            <SignIn />
          </Route>
          <Route exact path='/Profile'>
            <Profile/>
          </Route>  
          <Route exact path="/Signup">
            <SignIn register={true} />
          </Route>
          <Route exact path="/dashboard">
            <DashBoard/>
          </Route>
          <Route exact path="/verify/:id">
           <AccountVerfication/> 
          </Route> 
          <Route exact path="/forgotpassword">
            <ForgotPassword/>
          </Route>  
        </Router>
      </Container>
  );
}
export default App;
