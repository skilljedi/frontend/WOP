<h1>SkillJedi-FrontEnd Official Repository</h1>
<p>node js and npm should be installed</p>
<p>Frontend is developed using React Library and Backend is to be done in Node js</p>
<p>create-react-app is used to develop react apps,since it has inbuilt modules needed for development like development server,Babel e.t.c.</p>
<h3>Collaboration Instructions</h3>
<ul>
<li>Install git on your Computer by<a href="https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html">clicking here</a></li>
<li>Use SSH based Authentication for authenticating to Repo(Because of 2FA)<a href="https://docs.gitlab.com/ee/ssh/README.html#generating-a-new-ssh-key-pair">for more details</a></li>
<li>git clone git@gitlab.com:skilljedi/frontend/WOP.git</li>
<li>cd WOP</li>
<li>npm install</li>
<li>npm start</li>
<li>Create a new branch under your name and start coding based on<a href="https://www.figma.com/file/2cz8X4UpxMkKuJa2BWXstM/Site?node-id=0%3A1">Figma Prototype</a></li>
<li>Task Assigned to you can be found <a href="https://docs.google.com/spreadsheets/d/1dziyqeKdNpxDo_hQrj9Uxu7RzgU2lylDijXvvK5n_hM/edit#gid=1935943576">here</a></li>
</ul>
<p>This will run a server on localhost:3000</p>
<p>Feel Free to innovate(Let me know that)</p>
<h4>Go to the wiki tab for contribution instruction<h4>
<p><b>Go to the wiki tab for more details about project</b></p>
<h3>File Structure details</h3>
<p>src contains source code and other necessary things</p>
<ul>
<li><b>Components:-</b>Reusable component goes inside components
<br>Example Footer every page will have same footer so it's reusable. 
 </li>
<li><b>Pages:-</b>Those pages to be embedded in the Navbar to be added here<br>Example:Courses,Projects etc</li>  
<li><b>Styles:-</b>Css Of components and pages</li>
<li><b>Images:-</b>Images used in components and pages</li> 
<li><b>Redux:-</b>Actions,store and reducers used in the applications</li>  
</ul>  
